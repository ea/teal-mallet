#!/bin/sh
service nginx stop
if ! /opt/letsencrypt/letsencrypt-auto certonly -nvv --renew-by-default --standalone --email {{ letsencrypt_email }} -d {{ inventory_hostname }} > /var/log/letsencrypt/renew.log 2>&1 ; then
    echo Automated renewal failed:
    cat /var/log/letsencrypt/renew.log
    service nginx start
    exit 1
fi
service nginx start