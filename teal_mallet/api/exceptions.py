# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
from teal_mallet import logger


class BadRequestError(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
        self.status_code = 400
        logger.warning('{0}: {1}'.format(self.status_code, self.message))

    def to_dict(self):
        return {'message': self.message}


class UnknownContentTypeError(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
        self.status_code = 415
        logger.warning('{0}: {1}'.format(self.status_code, self.message))

    def to_dict(self):
        return {'message': self.message}


class ResourceNotFoundError(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
        self.status_code = 404
        logger.warning('{0}: {1}'.format(self.status_code, self.message))

    def to_dict(self):
        return {'message': self.message}


class InvalidDataError(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
        self.status_code = 403
        logger.warning('{0}: {1}'.format(self.status_code, self.message))

    def to_dict(self):
        return {'message': self.message}


class InternalServerError(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
        self.status_code = 500
        logger.warning('{0}: {1}'.format(self.status_code, self.message))

    def to_dict(self):
        return {'message': self.message}
