# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
from datetime import datetime
import json
import codecs
import hmac

from flask import request
from lxml import etree
import jsonschema

from teal_mallet.api.exceptions import InvalidDataError
from teal_mallet import app, logger


EA_regions = {
    'AN': 'Anglian',
    'CY': 'EA Wales',
    'MI': 'Midlands',
    'NE': 'North East',
    'NW': 'North West',
    'SO': 'Southern',
    'SW': 'South West',
    'TH': 'Thames',
    'EA': 'National Agency System',
    'MO': 'Met Office'
}

EA_systems = {
    'TS': 'Telemetry System',
    'FS': 'Forecasting System',
    'HY': 'Hyrad',
    'WI': 'Wiski',
    'XX': 'Not applicable'
}

AMON_to_EA_periods = {
    'INSTANT': 'Instantaneous',
    'CUMULATIVE': 'Cumulative Total',
    'PULSE': 'Event'
}

EA_to_AMON_periods = {
    'Instantaneous': 'INSTANT',
    'Cumulative Total': 'CUMULATIVE',
    'Event': 'PULSE',
    'Maximum': 'INSTANT',
    'Minimum': 'INSTANT',
    'Mean': 'INSTANT',
    'Total': 'CUMULATIVE'
}

xml_schema = etree.XMLSchema(etree.parse('teal_mallet/schemas/EATimeSeriesDataExchangeFormat.xsd'))

# TODO: This shouldn't be necessary, Python3.4 should handle utf8 files without problem. And indeed it does in this
# instance, *unless* we run it through supervisord, in which loading json with the regular Python open() causes the
# json module to assume it's in ASCII and baulk. I can't figure out what differences there are between running
# gunicorn from the command line, and running it via supervisord. If anyone has an idea, feel free to fix it!
with codecs.open('teal_mallet/schemas/AMON_schema.json', 'r', encoding='utf8') as json_file:
    json_schema = json.load(json_file)

xml_namespaces = {'md': 'http://www.environment-agency.gov.uk/XMLSchemas/EAMetadataFormat',
                  'ea': 'http://www.environment-agency.gov.uk/XMLSchemas/EATimeSeriesDataExchangeFormat'}


@app.template_filter('datetimeformat')
def datetimeformat_filter(value):
    return datetime.utcnow().strftime(value)


@app.template_filter('ea_to_amon')
def ea_to_amon_filter(value):
    return EA_to_AMON_periods[value]


def parse_ea_xml(xml):
    """
    Validate and parse a string as an EA Time Series XML file and return an AMON format to a form easily digestible to
    InfluxDB
    :param xml: a string containing an XML file.
    :return: The station data and the list of measurements in InfluxDB-readable format.
    """
    # noinspection PyArgumentList
    parser = etree.XMLParser(schema=xml_schema)

    root = etree.fromstring(xml, parser=parser)

    station_element = root.xpath('ea:Station', namespaces=xml_namespaces)[0]
    sets = station_element.xpath('ea:SetofValues', namespaces=xml_namespaces)

    station = {'reference': station_element.get('stationReference'), 'region': station_element.get('region'),
               'stationName': station_element.get('stationName'), 'ngr': station_element.get('ngr')}

    if sets is None:
        return station, None

    points = []

    # Ensure we don't have colons in the tags because we use that as a separator when generating the unique stream name.
    for s in sets:
        tags = dict()
        tags['parameter'] = s.get('parameter').replace(':', '_')
        tags['qualifier'] = s.get('qualifier').replace(':', '_')
        tags['unit'] = s.get('units').replace(':', '_')
        tags['period'] = s.get('period').replace(':', '_')
        tags['dataType'] = s.get('dataType').replace(':', '_')

        for v in s.iter('{{{0}}}Value'.format(xml_namespaces['ea'])):
            timestamp = datetime.strptime('{0} {1}'.format(v.get('date'), v.get('time')), '%Y-%m-%d %H:%M:%S')
            value = float(v.text)
            point = {'measurement': 'EnvironmentAgency', 'tags': tags, 'time': timestamp, 'fields': {'value': value}}
            points.append(point)

    return station, points


def parse_amon_json(document):
    """
    Validate and convert a Python dictionary in AMON format to a form easily digestible to InfluxDB

    :param document: An AMON format dictionary.
    :return: The station data and the list of measurements in InfluxDB-readable format.
    """
    jsonschema.validate(document, json_schema)

    device = document['devices'][0]

    if 'metadata' in device:
        station = {'reference': device['metadata']['stationReference'],
                   'region': device['metadata']['region'],
                   'stationName': device['metadata']['stationName'],
                   'ngr': device['metadata']['ngr']}
    else:
        station = {}

    if 'measurements' not in device:
        return station, None

    points = []

    for measurement in device['measurements']:
        measurement_type = measurement['type']
        reading = None

        for r in device['readings']:
            if r['type'] == measurement_type:
                reading = r
                break

        if reading is None:
            raise ValueError('Unable to find type "{0}" in the list of AMON readings.'.format(measurement_type))

        tags = dict()
        t = measurement_type.split(':')
        tags['parameter'] = t[0]

        if len(t) > 1:
            tags['period'] = t[1]
        else:
            tags['period'] = 'Unspecified'

        if len(t) > 2:
            tags['qualifier'] = t[2]

        # We appear to have a name with a colon in it, which is illegal because we use colons as a separator when
        # generating the unique stream name.
        if len(t) > 3:
            raise ValueError('Inavlid measurement type "{0}". A name cannot contain ":"'.format(measurement_type))

        tags['unit'] = reading['unit']
        tags['dataType'] = AMON_to_EA_periods[reading['period']]

        try:
            fields = {'value': float(measurement['value'])}
        except KeyError:
            fields = {'error': measurement['error']}

        point = {'measurement': 'EnvironmentAgency', 'tags': tags,
                 'time': measurement['timestamp'], 'fields': fields}
        points.append(point)

    return station, points


def sanitize_influx_sql(*args):
    """
    Escapes single quotes so InfluxDB handles them properly and reduces the chance of SQL injection.

    :param args: A list of strings to be sanitized.
    :return: A sanitized list of strings.
    """
    return [x.replace("'", "\\'") for x in args]


def validate_hmac(hmac_key):
    if hmac_key == '':
        return

    if 'Content-HMAC' in request.headers:
        hash_func = None
        try:
            if request.content_length > app.config['HMAC_MAX_CONTENT_LENGTH']:
                message = 'Unwilling to hash content of size {0} bytes ' \
                          'because it is too long.'.format(request.content_length)
                logger.warning(message)
                raise InvalidDataError(message)

            hash_func, hmac_digest = request.headers['Content-HMAC'].trim().split()[:2]
            h = hmac.new(hmac_key, request.get_data(), hash_func)

            if h.hexdigest() == hmac_digest.lower():
                return
            else:
                message = 'Content-HMAC header doesn\'t match content.'
                raise InvalidDataError(message)
        except ValueError:
            message = 'Unknown HMAC hash function "{0}". Ignoring request.'.format(hash_func)
            logger.warning(message)
            raise InvalidDataError(message)
