# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
import json
from datetime import datetime, timedelta

import influxdb
import strict_rfc3339
from flask.ext.restful import marshal
from influxdb.exceptions import InfluxDBClientError, InfluxDBServerError
from lxml import etree
from flask import render_template, request, Blueprint, jsonify, make_response, current_app
from sqlalchemy import orm, func, exc, or_
from flask_security import current_user

from teal_mallet import db, influx_db, logger
from teal_mallet.api.exceptions import UnknownContentTypeError, ResourceNotFoundError, InternalServerError,\
    InvalidDataError, BadRequestError
from teal_mallet.api.util import parse_ea_xml, parse_amon_json, sanitize_influx_sql, validate_hmac
from teal_mallet.models.api import Station, Metadata
from teal_mallet.models.security import User
from teal_mallet.util import TheJSONEncoder

api_blueprint = Blueprint('api', __name__, template_folder='templates')


@api_blueprint.errorhandler(UnknownContentTypeError)
def handle_unknown_content_type(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code

    return response


@api_blueprint.errorhandler(BadRequestError)
def handle_bad_request_type(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code

    return response


@api_blueprint.errorhandler(ResourceNotFoundError)
def handle_resource_not_found(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code

    return response


@api_blueprint.errorhandler(InternalServerError)
def handle_internal_server_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code

    return response


@api_blueprint.errorhandler(InvalidDataError)
def handle_invalid_data_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code

    return response


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station/<string:station_id>', methods=['GET'])
def get_station(entity_id, station_id):
    """
    Get station details for a given station.

    :param entity_id:
    :param station_id:
    :return:
    """
    entity_id, station_id = sanitize_influx_sql(entity_id, station_id)

    try:
        geom_column = func.ST_AsGeoJSON(func.ST_Transform(Station.location, 4326)).label('geom')
        station = db.session.query(Station, geom_column).filter(
            Station.id == station_id, or_(Station.is_private == False, Station.owner_id == current_user.id)).one()
    except orm.exc.MultipleResultsFound:
        raise InternalServerError("Multiple stations found with the same ID '{0}'".format(station_id))
    except orm.exc.NoResultFound:
        raise ResourceNotFoundError("Unable to find station '{0}'".format(station_id))

    metadata = Metadata.query.filter().first()

    if metadata is None:
        raise InternalServerError('Metadata has not been set up')

    accepted_types = ('application/vnd.geo+json', 'application/xml', 'application/json')
    mimetype = request.accept_mimetypes.best_match(accepted_types)

    if mimetype == 'application/xml':
        data = render_template('EA.xml', metadata=metadata, station=station[0])
    elif mimetype == 'application/json':
        data = render_template('AMON.json', metadata=metadata, station=station[0])
    elif mimetype == 'application/vnd.geo+json':
        return _render_station_as_geojson(metadata, station)
    else:
        raise UnknownContentTypeError("Cannot handle MIME type '{0}' expected "
                                      "{1}".format(request.accept_mimetypes, accepted_types))

    response = make_response(data)
    response.headers['Content-Type'] = mimetype

    return response


# noinspection PyUnusedLocal
def _render_station_as_geojson(metadata, result):
    # TODO: Make this into a template.
    station = result[0]

    count = influx_db.query("select count(value) from EnvironmentAgency where station = '{0}'".format(station.id))

    query = "select dataType, parameter, period, qualifier, unit, value from EnvironmentAgency " \
            "where station = '{0}' " \
            "group by dataType, parameter, period, qualifier, unit order by time desc limit 1".format(station.id)

    last_entry = influx_db.query(query)
    try:
        station_point_count = count.get_points().__next__()
        last_point = last_entry.get_points().__next__()

        point_count = station_point_count['count']
        point_time = last_point['time']
        series = last_entry.raw['series']
    except StopIteration:
        point_count = 0
        point_time = None
        series = []

    s = {
        'type': 'Feature',
        'properties': {
            'name': station.name, 'count': point_count, 'reference': station.reference,
            'ngr': station.ngr, 'id': station.id, 'time': point_time, 'region': station.region,
            'series': series
        },
        'geometry': json.loads(result[1])
    }

    data = {'type': 'FeatureCollection', 'features': [s]}
    response = make_response(json.dumps(data, cls=TheJSONEncoder))
    response.headers['Content-Type'] = 'application/vnd.geo+json'

    return response


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station', methods=['POST'])
# @hmac_content
def post_station(entity_id):
    """
    Handles XML and JSON POST operations for measurements and stations.

    :param entity_id:

    :raises: InvalidDataError
    :raises: UnknownContentTypeError
    :raises: InternalServerError
    """
    entity_id = sanitize_influx_sql(entity_id)

    if request.content_type == 'application/xml':
        try:
            logger.info('Loading XML for new station')
            data, measurements = parse_ea_xml(request.data)
        except etree.XMLSyntaxError:
            raise InvalidDataError('The data provided is not valid EA Time Series XML')

    elif request.content_type == 'application/json':
        try:
            logger.info('Loading JSON for new station')
            data, measurements = parse_amon_json(request.json)
        except ValueError as e:
            raise InvalidDataError(e)
    else:
        raise UnknownContentTypeError("Cannot handle MIME type '{0}' "
                                      "expected 'application/xml' or 'application/json'".format(request.content_type))

    station = Station(data.get('reference'), data.get('region'),
                      data.get('stationName'), data.get('ngr'))
    db.session.add(station)
    db.session.commit()

    if measurements is not None:
        validate_hmac(station.hmac_key)
        _store_measurements(station.id, measurements)

    response = jsonify({'station': station.id})
    response.status_code = 201

    return response


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station/<string:station_id>/measurements', methods=['POST'])
def post_measurements(entity_id, station_id):
    entity_id, station_id = sanitize_influx_sql(entity_id, station_id)

    if request.content_type == 'application/xml':
        try:
            logger.info('Loading XML for new station')
            _, measurements = parse_ea_xml(request.data)
        except etree.XMLSyntaxError:
            raise InvalidDataError('The data provided is not valid EA Time Series XML')

    elif request.content_type == 'application/json':
        try:
            logger.info('Loading JSON for new station')
            _, measurements = parse_amon_json(request.json)
        except ValueError as e:
            raise InvalidDataError(e)
    else:
        raise UnknownContentTypeError("Cannot handle MIME type '{0}' "
                                      "expected 'application/xml' or 'application/json'".format(request.content_type))

    try:
        station = Station.query.filter(Station.id == station_id).one()
    except orm.exc.MultipleResultsFound:
        raise InternalServerError("Multiple stations found with the same ID '{0}'".format(station_id))
    except orm.exc.NoResultFound:
        raise ResourceNotFoundError("Unable to find station '{0}'".format(station_id))

    if measurements is not None:
        validate_hmac(station.hmac_key)
        _store_measurements(station.id, measurements)

    if request.args.get('ftp', 'false') == 'true':
        with current_app.test_client() as client:
            headers = dict(request.headers)
            headers['Accept'] = 'application/xml'
            query = {'start': measurements[0]['time'], 'end': measurements[-1]['time']}
            r = client.get('/api/{0}/station/{1}/measurements'.format(entity_id, station_id),
                           headers=headers, query_string=query)
            file_name = 'NWTSNWWI{0}.xml'.format(datetime.utcnow().strftime('%Y%m%d%H%M%S'))

            with open('/srv/ftp/teal-mallet/{0}'.format(file_name), 'w') as f:
                f.write(r.data.decode('utf-8'))

    response = jsonify({'station': station.id})
    response.status_code = 201

    return response


def _store_measurements(station_id, measurements):
    try:
        influx_db.write_points(measurements, tags={'station': station_id}, time_precision='s')
    except InfluxDBClientError as e:
        raise InvalidDataError("The time-series data provided cannot be stored in the database: '{0}'".format(str(e)))
    except InfluxDBServerError as e:
        raise InternalServerError("Unable to connect to InfluxDB: '{0}'".format(str(e)))


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station', methods=['GET'])
def station_summary(entity_id):
    entity_id, = sanitize_influx_sql(entity_id)

    srid = int(request.args.get('srid', '4326'))
    mimetype = request.accept_mimetypes.best_match(('application/vnd.geo+json', 'application/vnd.google-earth.kml+xml'))

    if mimetype == 'application/vnd.geo+json':
        fmt = func.ST_AsGeoJSON
    elif mimetype == 'application/vnd.google-earth.kml+xml':
        fmt = func.ST_AsKML
    else:
        fmt = func.ST_AsGeoJSON
        # raise UnknownContentTypeError("Cannot handle MIME type '{0}' expected 'application/vnd.geo+json' or "
        #                               "'application/vnd.google-earth.kml+xml'".format(request.accept_mimetypes))

    if entity_id == 'public':
        station_results = db.session.query(Station, fmt(func.ST_Transform(Station.location, srid)), User.name).\
            join(User, Station.owner_id == User.id).\
            filter(Station.is_private == False).all()
    else:
        station_results = db.session.query(Station, fmt(func.ST_Transform(Station.location, srid)), User.name).\
            join(User, Station.owner_id == User.id).\
            filter(or_(Station.is_private == False, Station.owner_id == current_user.id)).all()
    stations = list()
    bbox = [9999.0, 9999.0, -9999.0, -9999.0]

    for result in station_results:
        station = result[0]

        s = {'type': 'Feature', 'geometry': json.loads(result[1])}

        if s['geometry']['coordinates'][0] < bbox[0]:
            bbox[0] = s['geometry']['coordinates'][0]

        if s['geometry']['coordinates'][1] < bbox[1]:
            bbox[1] = s['geometry']['coordinates'][1]

        if s['geometry']['coordinates'][0] > bbox[2]:
            bbox[2] = s['geometry']['coordinates'][0]

        if s['geometry']['coordinates'][1] > bbox[3]:
            bbox[3] = s['geometry']['coordinates'][1]

        properties = dict(marshal(station, Station.resource_fields))
        count = influx_db.query("select count(value) from EnvironmentAgency where station = '{0}'".format(station.id))
        last_entry = influx_db.query("select * from EnvironmentAgency "
                                     "where station = '{0}' order by time desc limit 1".format(station.id))
        properties['owner'] = result[2]
        try:
            station_point_count = count.get_points().__next__()
            last_point = last_entry.get_points().__next__()
            last_time = datetime.strptime(last_point['time'], '%Y-%m-%dT%H:%M:%SZ')
            properties.update({'count': station_point_count['count'], 'time': last_time})
        except StopIteration:
            pass

        s.update({'properties': properties})
        stations.append(s)

    data = json.dumps({'type': 'FeatureCollection', 'features': stations, 'bbox': bbox}, cls=TheJSONEncoder)
    response = make_response(data)
    response.headers['Content-Type'] = 'application/vnd.geo+json'

    return response


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station/<string:station_id>/measurements', methods=['GET'])
def station_measurements(entity_id, station_id):
    entity_id, station_id = sanitize_influx_sql(entity_id, station_id)
    metadata = Metadata.query.filter().first()

    if metadata is None:
        raise InternalServerError('Metadata has not been set up')

    try:
        results = db.session.query(Station, Station.location.ST_AsGeoJSON()).filter(Station.id == station_id).one()
        station = results[0]
    except orm.exc.MultipleResultsFound:
        raise InternalServerError("Multiple stations found with the same ID '{0}'".format(station_id))
    except orm.exc.NoResultFound:
        raise ResourceNotFoundError("Unable to find station '{0}'".format(station_id))
    except exc.StatementError:
        raise InternalServerError("SQL error for station '{0}'".format(station_id))

    try:
        readings_query = "show series from EnvironmentAgency where station=~ /{0}/".format(station_id)
        readings_result = influx_db.query(readings_query)
    except influxdb.exceptions.InfluxDBClientError as e:
        raise ResourceNotFoundError(e.content)

    readings = list()
    measurements = list()

    if len(readings_result) > 0:
        for value in readings_result.raw['series'][0]['values']:
            readings.append(dict(zip(readings_result.raw['series'][0]['columns'], value)))

        now = datetime.utcnow()

        start_date = request.args.get('start', '')
        end_date = request.args.get('end', '')

        if start_date != 'all':
            if not strict_rfc3339.validate_rfc3339(start_date):
                start_date = (now - timedelta(days=1)).strftime('%Y-%m-%dT%H:%M:%SZ')

            if not strict_rfc3339.validate_rfc3339(end_date):
                end_date = now.strftime('%Y-%m-%dT%H:%M:%SZ')
        else:
            first_entry = influx_db.query("select time, value from EnvironmentAgency "
                                          "where station = '{0}' order by time asc limit 1".format(station_id))
            first_point = first_entry.get_points().__next__()
            start_date = first_point['time']

            last_entry = influx_db.query("select time, value from EnvironmentAgency "
                                         "where station = '{0}' order by time desc limit 1".format(station_id))
            last_point = last_entry.get_points().__next__()
            end_date = last_point['time']

        station.start = start_date
        station.end = end_date

        if request.args.get('header', 'false') == 'false':
            reading_type, = sanitize_influx_sql(request.args.get('type', '.*'))
            # TODO: Handle the error column.
            measurements_query = "select dataType, parameter, period, qualifier, unit, value from EnvironmentAgency " \
                                 "where station = '{0}' " \
                                 "and time > '{1}' " \
                                 "and time <= '{2}' " \
                                 "and parameter =~ /{3}/ " \
                                 "group by dataType, parameter, " \
                                 "period, qualifier, unit".format(station_id, start_date, end_date, reading_type)

            measurements_result = influx_db.query(measurements_query)

            measurements = measurements_result.raw

    accepted_types = ('application/xml', 'application/json', 'text/plain')
    mimetype = request.accept_mimetypes.best_match(accepted_types)

    if mimetype == 'application/xml':
        data = render_template('EA.xml', metadata=metadata, station=station, readings=readings,
                               measurements=measurements)
    elif mimetype == 'application/json':
        data = render_template('AMON.json', metadata=metadata, station=station, readings=readings,
                               measurements=measurements)
    elif mimetype == 'text/plain':
        data = render_template('plain.txt', metadata=metadata, station=station, readings=readings,
                               measurements=measurements)
    else:
        raise UnknownContentTypeError("Cannot handle MIME type '{0}' expected "
                                      "{1}".format(request.accept_mimetypes, accepted_types))

    response = make_response(data)
    response.headers['Content-Type'] = mimetype

    return response


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station/<string:station_id>/measurements/<string:stream_name>',
                     methods=['DELETE'])
def delete_stream(entity_id, station_id, stream_name):
    entity_id, station_id, stream_name = sanitize_influx_sql(entity_id, station_id, stream_name)

    try:
        station = db.session.query(Station).filter(Station.id == station_id).one()
        if 'administrator' not in current_user.roles and current_user.id != station.owner_id:
            raise InvalidDataError("User '{0}' does not have sufficient rights to delete stream '{1}' "
                                   "from station '{2}'".format(current_user.id, stream_name, station_id))
    except orm.exc.MultipleResultsFound:
        raise InternalServerError("Multiple stations found with the same ID '{0}'".format(station_id))
    except orm.exc.NoResultFound:
        raise ResourceNotFoundError("Unable to find station '{0}'".format(station_id))

    try:
        parameter, qualifier, data_type, unit, period = stream_name.split(':')
    except ValueError:
        raise BadRequestError('An invalid stream key was given: "{0}"'.format(stream_name))

    delete_query = "drop series from EnvironmentAgency " \
                   "where station = '{0}' " \
                   "and parameter = '{1}' " \
                   "and qualifier = '{2}' " \
                   "and dataType = '{3}' " \
                   "and unit = '{4}' " \
                   "and period = '{5}' ".format(station_id, parameter, qualifier, data_type, unit, period)

    delete_result = influx_db.query(delete_query)

    return '', 204


# noinspection PyUnusedLocal
@api_blueprint.route('/<string:entity_id>/station/<string:station_id>', methods=['DELETE'])
def delete_station(entity_id, station_id):
    entity_id, station_id = sanitize_influx_sql(entity_id, station_id)

    try:
        station = db.session.query(Station).filter(Station.id == station_id).one()
        if 'administrator' not in current_user.roles and current_user.id != station.owner_id:
            raise InvalidDataError("User '{0}' does not have sufficient rights to delete station '{1}'".
                                   format(current_user.id, station_id))
    except orm.exc.MultipleResultsFound:
        raise InternalServerError("Multiple stations found with the same ID '{0}'".format(station_id))
    except orm.exc.NoResultFound:
        raise ResourceNotFoundError("Unable to find station '{0}'".format(station_id))

    try:
        delete_query = "drop series from EnvironmentAgency where station = '{0}'".format(station.id)
        delete_result = influx_db.query(delete_query)
    except InfluxDBClientError:
        logger.warning('Measurement EnvironmentAgency does not exist.'
                       'This is probably because no readings have been taken yet.')

    db.session.delete(station)
    db.session.commit()

    return '', 204
