/**
# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
*/

var map;
var gs = [];

function initMap() {
    $.ajax({
        url: '/api/a3b40af1-16b5-4727-bcdc-60b0816a1e7b/station',
        data: {srid: 4326},
        type: 'GET',
        headers: {
            Accept: 'application/vnd.geo+json'
        }
    }).done(function(data) {
        map = new google.maps.Map(document.getElementById('map'));
        var centre = {lat: 0.0, lng: 0.0};
        var set_centre = false;

        if (typeof station_id !== 'undefined') {
            for (var i = 0; i < data.features.length; i++) {
                if (data.features[i].properties.id === station_id) {
                    centre = {lat: data.features[i].geometry.coordinates[1], lng: data.features[i].geometry.coordinates[0]};
                    set_centre = true;
                    break;
                }
            }
        }

        if (set_centre) {
            map.setCenter(centre);
            map.setZoom(15);
        } else {
            var southWest = new google.maps.LatLng(data.bbox[1], data.bbox[0]);
            var northEast = new google.maps.LatLng(data.bbox[3], data.bbox[2]);
            var bounds = new google.maps.LatLngBounds(southWest, northEast);
            map.fitBounds(bounds);
        }

        map.data.setStyle(function(feature) {
            return {
                title: feature.getProperty('name'),
                icon: (typeof station_id !== 'undefined' && feature.getProperty('id') === station_id) ? '/static/red_marker.png' : '/static/green_marker.png',
                optimized: false
            };
        });

        map.data.addGeoJson(data);
    });
}

function format_chart_title(title) {
    var parts = title.split(':');

    if (parts.length < 3) {
        return title;
    }

    return parts[0] + (parts[2] === '' ? '':' (' + parts[2] + ')');
}

function get_measurements(start) {
    $.ajax({
        url: '/api/a3b40af1-16b5-4727-bcdc-60b0816a1e7b/station/' + station_id + '/measurements',
        data: {start: start},
        type: 'GET',
        headers: {
            Accept: 'application/json'
        }
    }).done(function(data) {
        // TODO: Corner case: If a new stream is added to the station and the user changes the date range, we could get
        // out of sync because there wouldn't be enough charts to show each stream.
        for (var index = 0; index < gs.length; ++index) {
            var reading = data.devices[0].readings[index];

            var measurements = [[new Date(data.devices[0].metadata.start), null]];

            $.each(data.devices[0].measurements, function(index, measurement) {
                if (measurement.type === reading.type) {
                    measurements.push([new Date(measurement.timestamp), measurement.value]);
                }
            });

            measurements.push([new Date(data.devices[0].metadata.end), null]);

            gs[index].updateOptions({
                file: measurements,
                title: format_chart_title(reading.type),
                labels: ['time', 'value'],
                ylabel: reading.unit,
                showRangeSelector: true,
                fillGraph: false
            });
            gs[index].resetZoom();
        }
    });
}

function init_graph(container_id) {
    var start = 'all';
    $.ajax({
        url: '/api/a3b40af1-16b5-4727-bcdc-60b0816a1e7b/station/' + station_id + '/measurements',
        data: {start: start},
        type: 'GET',
        headers: {
            Accept: 'application/json'
        }
    }).done(function(data) {
        if (data === undefined || data.devices[0].readings === undefined) {
            return;
        }

        var graph_container = $('div#' + container_id);
        var device = data.devices[0];

        $.each(device.readings, function(index, reading) {
            var container = $('<div id="graph-' + index + '" class="graph"></div>');
            graph_container.append(container);

            var measurements = [[new Date(data.devices[0].metadata.start), null]];

            // TODO: This is super slow brute force. Give it more schmoo.
            $.each(device.measurements, function(index, measurement) {
                if (measurement.type === reading.type) {
                    measurements.push([new Date(measurement.timestamp), measurement.value]);
                }
            });

            measurements.push([new Date(data.devices[0].metadata.end), null]);

            gs.push(new Dygraph(
                container[0],
                measurements,
                {
                    title: format_chart_title(reading.type),
                    labels: ['time', 'value'],
                    ylabel: reading.unit,
                    showRangeSelector: true,
                    fillGraph: false
                }
            ));
        });

        if (gs.length > 1) {
            Dygraph.synchronize(gs, {zoom_horizontal: true, zoom_vertical: false, selection: true});
        }
    });
}

$(function() {
    $('[data-toggle="tooltip"]').tooltip();

    var clipboard = new Clipboard('.btn');

    clipboard.on('success', function(e) {
        $(e.trigger).attr('title', 'Copied!').tooltip('fixTitle').tooltip('show');
        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        $(e.trigger).attr('title', 'Press Ctrl+C to copy').tooltip('fixTitle').tooltip('show');
    });

    $('.table tr[data-href]').each(function() {
        $(this).css('cursor', 'pointer').click(function() {
            var href = $(this).attr('data-href');

            if (href.charAt(0) === '#') {
                //document.getElementById(href);
                var wibble = $('div' + href)[0];
                wibble.scrollIntoView();
            } else {
                document.location = href;
            }
        });
    });

    $('ul#dropdown-menu-export-items li').click(function() {
        var accept = 'application/json';
        var filename = '';
        var header = false;

        if (this.id === 'export-EA') {
            accept = 'application/xml';
            filename = station_id + '.xml';
            header = false;
        } else if (this.id === 'export-AMON') {
            accept = 'application/json';
            filename = station_id + '.json';
            header = false;
        } else if (this.id === 'export-config') {
            accept = 'application/json';
            filename = station_id + '_conf.json';
            header = true;
        }

        var start;
        var end;

        if (header || gs[0] === undefined) {
            start = moment();
            end = moment();
        } else {
            start = new moment(gs[0].xAxisRange()[0]);
            end = new moment(gs[0].xAxisRange()[1]);
        }

        $.ajax({
            url: '/api/a3b40af1-16b5-4727-bcdc-60b0816a1e7b/station/' + station_id + '/measurements',
            data: {start: start.format(), end: end.format(), header: header},
            type: 'GET',
            headers: {
                Accept: accept
            }
        }).done(function(data) {
            var b;
            if (accept === 'application/json') {
                b = new Blob([JSON.stringify(data)], {type: accept});
            } else if (accept === 'application/xml') {
                b = new Blob([(new XMLSerializer()).serializeToString(data)], {type: accept});
            }
            saveAs(b, filename);
        }).fail(function(xhr, status, error) {
            console.log('Fail');
            console.log(status);
            console.log(error);
        });
    });

    $('[data-toggle="popover"]').popover({container: 'body'});

    // Clear the new station form for next time.
    $('#new-station-modal').on('hidden.bs.modal', function () {
        $(this)
        .find("input.form-control")
           .val('')
           .end()
        .find("input[type=checkbox], input[type=radio]")
           .prop("checked", "")
           .end();
    });

    $('ul#dropdown-menu-date-range-items li').on('click', function() {
        if (this.dataset.unit !== "") {
            var start = moment().subtract(this.dataset.duration, this.dataset.unit);
            start.utc();
            get_measurements(start.format());
        } else {
            get_measurements('all');
        }
    });

    $('#delete-stream-button').on('click', function() {
        var stream = $(this).data('stream');

        $.ajax({
            url: '/api/a3b40af1-16b5-4727-bcdc-60b0816a1e7b/station/' + station_id + '/measurements/' + stream,
            type: 'DELETE'
        }).done(function() {
            $('#delete-stream-button').data('stream', '');
            $('#delete-stream-modal').modal('hide');
            location.reload(true);
        });
    });

    $('td.delete-datastream').on('click', function() {
        var stream = $(this).parent().data('stream');
        $('#delete-stream-text').html('<p>Are you sure you want to delete the stream <b>'+ format_chart_title(stream) + '</b>?</p>');
        $('#delete-stream-button').data('stream', stream);
        $('#delete-stream-modal').modal();
        // Swallow the event so we don't follow the href bookmark.
        return false;
    });

    $('#delete-station-button').on('click', function() {
        var station = $(this).data('station-id');

        $.ajax({
            url: '/api/a3b40af1-16b5-4727-bcdc-60b0816a1e7b/station/' + station,
            type: 'DELETE'
        }).done(function() {
            $('#delete-station-button').data('station-id', '');
            $('#delete-station-modal').modal('hide');
            location.reload(true);
        });
    });

    $('td.delete-station').on('click', function() {
        var station = $(this).parent().data('station-id');
        var station_name = $(this).parent().data('station-name');
        $('#delete-station-text').html('<p>Are you sure you want to delete station <b>'+ station_name + '</b>?</p>' +
            '<p>Warning: This will delete all measurements and readings taken by this station.</p>');
        $('#delete-station-button').data('station-id', station);
        $('#delete-station-modal').modal();
        // Swallow the event so we don't follow the href bookmark.
        return false;
    });
});
