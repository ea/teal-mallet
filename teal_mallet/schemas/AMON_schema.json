{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "AMON v3.1",
  "description": "Schema for the AMEE AMON v3.1 sensor data exchange format. See: https://amee.github.io/AMON/",
  "type": "object",
  "definitions": {
    "id": {
      "description": "A UUID. Can be any version.",
      "type": "string",
      "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$"
    }
  },
  "properties": {
    "meteringPoints": {
      "description": "In the AMON data format, the “meteringPoints” section is used to represent physical or virtual metering points. Note that because the relationship between a “device” and a “meteringPoint” is defined in the “device” section of the data format, a “meteringPoint” may have one or more “devices”; but a “device” may belong to at most one “meteringPoint”.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "meteringPointId": {
            "description": "A UUID for the “meteringPoint”. Required for a “meteringPoint”; however, systems that implement the AMON data format may relax this requirement to make the field optional for AMON formatted messages that are requesting that a “meteringPoint” be created.",
            "$ref": "#/definitions/id"
          },
          "entityId": {
            "description": "A UUID for the “entity”. Required for an “entity”; however, systems that implement the AMON data format may relax this requirement to make the field optional for AMON formatted messages that are requesting than an “entity” be created.",
            "$ref": "#/definitions/id"
          },
          "description": {
            "description": "An optional textual description of the metering point.",
            "type": "string"
          },
          "metadata": {
            "description": "An optional JSON object of metadata about the “meteringPoint”. This allows the AMON data format to handle any type of metadata relating to the “device”.",
            "type": "object"
          }
        },
        "required": ["meteringPointId", "entityId"]
      }
    },
    "entities": {
      "description": "In the AMON data format, the “entities” section is used to represent physical or virtual entities which may have a relationship with a “device” or “meteringPoint”.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "entityId": {
            "description": "A UUID for the “entity”. Required for an “entity”; however, systems that implement the AMON data format may relax this requirement to make the field optional for AMON formatted messages that are requesting than an “entity” be created.",
            "$ref": "#/definitions/id"
          },
          "deviceIds": {
            "description": "An array of “device” UUIDs, representing the “devices” that belong to the “entity”.",
            "type": "array",
            "items": {
              "$ref": "#/definitions/id"
            }
          },
          "meteringPointIds": {
            "description": "An array of “meteringPoint” UUIDs, representing the “meteringPoints” that belong to the “entity”.",
            "type": "array",
            "items": {
              "$ref": "#/definitions/id"
            }
          }
        },
        "required": ["entityId"]
      }
    },
    "devices": {
      "description": "The “devices” section is used for representing physical or virtual metering/monitoring devices and their data.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "deviceId": {
            "description": "A UUID for the “device”. Required for a “device”; however, systems that implement the AMON data format may relax this requirement to make the field optional for AMON formatted messages that are requesting that a “device” be created.",
            "$ref": "#/definitions/id"
          },
          "entityId": {
            "description": "A UUID for the “entity”. Required for an “entity”; however, systems that implement the AMON data format may relax this requirement to make the field optional for AMON formatted messages that are requesting than an “entity” be created.",
            "$ref": "#/definitions/id"
          },
          "parentId": {
            "description": "A UUID for the device’s “parent”. Presence of this value indicates this device is a sub-meter.",
            "$ref": "#/definitions/id"
          },
          "meteringPointId": {
            "description": "An optional UUID of a “meteringPoint”, if this “device” is to be considered part of that “meteringPoint”.",
            "$ref": "#/definitions/id"
          },
          "description": {
            "description": "An optional textual description of the device. Commonly used for an in-house device ID and/or other useful identifier.",
            "type": "string"
          },
          "privacy": {
            "description": "Should the information about this device and its data be considered private, or public? Optional – systems that implement the AMON data format should assume a default of “private” if not specified.",
            "enum": ["private", "public"]
          },
          "location": {
            "description": "",
            "type": "object",
            "properties": {
              "name": {
                "description": "Optional textual description of the location of the “device”.",
                "type": "string"
              },
              "latitude": {
                "description": "Optional latitude of the “device”.",
                "type": "number",
                "minimum": -90.0,
                "maximum": 90.0
              },
              "longitude": {
                "description": "Optional longitude of the “device”.",
                "type": "number",
                "minimum": -180.0,
                "maximum": 180.0
              }
            }
          },
          "metadata": {
            "description": "An optional JSON object of metadata about the “device”. This allows the AMON data format to handle any type of metadata relating to the “device”.",
            "type": "object"
          },
          "readings": {
            "description": "The “readings” section defines what type of readings the “device” validly produces. An array of zero or more sets of values.",
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "type": {
                  "description": "A required string, defining a name for the type of “reading”. A set of standard reading types is listed below; however, the AMON data format does not specify any requirement regarding reading types. While it is recommended that the standard reading types be used, users of the data format are free to define and use their own type definitions, as appropriate to their devices and data.",
                  "type": "string"
                },
                "unit": {
                  "description": "Optional string, defining the unit for the “reading”. Units must be a valid unit as defined by the JScience library.",
                  "type": "string"
                },
                "resolution": {
                  "description": "Optional number, defining the number of seconds between each expected measurement.",
                  "type": "number"
                },
                "accuracy": {
                  "description": "Optional number, defining the accuracy of the “reading”.",
                  "type": "number"
                },
                "period": {
                  "description": "Optional string, defining the type of “reading”. May be one of “INSTANT”, “CUMULATIVE” or “PULSE”. Systems that implement the AMON data format should assume a default of “INSTANT” if not supplied.",
                  "enum": ["INSTANT", "CUMULATIVE", "PULSE"],
                  "default": "INSTANT"
                },
                "min": {
                  "description": "Optional number, defining the minimum valid value for the data.",
                  "type": "number"
                },
                "max": {
                  "description": "Optional number, defining the maximum valid value for the data.",
                  "type": "number"
                },
                "correction": {
                  "description": "Optional boolean, defining whether a correction factor has been applied to the data.",
                  "type": "boolean"
                },
                "correctedUnit": {
                  "description": "Optional string, containing the corrected unit type.",
                  "type": "string"
                },
                "correctionFactor": {
                  "description": "Optional number, defining the correction factor applied.",
                  "type": "number"
                },
                "correctionFactorBreakdown": {
                  "description": "Optional string, defining the process for obtaining the correction factor.",
                  "type": "string"
                }
              },
              "required": ["type"]
            }
          },
          "measurements": {
            "description": "The “measurements” section defines actual data measurements from the “device”. An array of zero or more sets of values.",
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "type": {
                  "description": "A required string, referencing a “reading” type that is defined for the “device”. All data measurements supplied for a “device” must use a “reading” “type” that has been defined for the “device”.",
                  "type": "string"
                },
                "timestamp": {
                  "description": "RFC 3339 string, required. The date/time that the “measurement” was produced.",
                  "type": "string",
                  "pattern": "^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}(?:\\.\\d{1,})?((?:[\\+\\-]\\d{2}:\\d{2})|Z)$"
                },
                "value": {
                  "description": "Optional number, boolean or string, being the actual “measurement” value.",
                  "type": ["number", "boolean", "string"]
                },
                "error": {
                  "description": "Optional string, describing an error condition if no “value” is present.",
                  "type": "string"
                },
                "aggregated": {
                  "description": "Optional boolean, set to true if the measurement data being described/exchanged has been aggregated (i.e. is not individual raw data values, but has been aggregated to reduce the number of “measurement” items that need to be listed).",
                  "type": "boolean"
                }
              },
              "required": ["type", "timestamp"]
            }
          }
        },
        "required": ["deviceId", "entityId"]
      }
    }
  }
}