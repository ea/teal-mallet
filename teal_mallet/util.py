# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
import logging
import uuid

import yaml
from flask.json import JSONEncoder
from yaml.parser import MarkedYAMLError
from sqlalchemy.types import TypeDecorator, BINARY
from sqlalchemy.dialects.postgresql import UUID as PSQL_UUID

logger = logging.getLogger('teal-mallet')


def british2xy(grid_ref):
    """
    Convert grid reference to coordinates.
    Lifted from http://snorf.net/blog/2014/08/12/converting-british-national-grid-and-irish-grid-references/

    :param false_easting: The false easting for this projection.
    :param false_northing:
    :param grid_sizes:
    :param grid_ref:
    :return:
    """

    alphabet = 'ABCDEFGHJKLMNOPQRSTUVWXYZ'

    # false easting and northing
    easting = -1000000
    northing = -500000

    letter = grid_ref[0]
    idx = alphabet.index(letter)
    col = (idx % 5)
    row = 4 - (idx // 5)
    easting += col * 500000
    northing += row * 500000

    letter = grid_ref[1]
    idx = alphabet.index(letter)
    col = (idx % 5)
    row = 4 - (idx // 5)
    easting += col * 100000
    northing += row * 100000

    # numeric components of grid reference
    grid_ref = grid_ref[2:]
    e = '{:0<5}'.format(grid_ref[0:len(grid_ref) // 2])
    e = '{}.{}'.format(e[0:5], e[5:])
    n = '{:0<5}'.format(grid_ref[len(grid_ref) // 2:])
    n = '{}.{}'.format(n[0:5], n[5:])
    easting += float(e)
    northing += float(n)

    return easting, northing


class UUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses Postgresql's UUID type, otherwise uses BINARY(16), to store the UUID.
    """
    impl = BINARY

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(PSQL_UUID())
        else:
            return dialect.type_descriptor(BINARY(16))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                if isinstance(value, bytes):
                    value = uuid.UUID(bytes=value)
                elif isinstance(value, int):
                    value = uuid.UUID(int=value)
                elif isinstance(value, str):
                    value = uuid.UUID(value)

        if dialect.name == 'postgresql':
            return str(value)
        else:
            return value.bytes

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        if dialect.name == 'postgresql':
            return uuid.UUID(value)
        else:
            return uuid.UUID(bytes=value)


class TheJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return str(obj)
        else:
            return JSONEncoder.default(self, obj)


class YAMLError(Exception):
    def __init__(self, message):
        self._message = message

    @property
    def message(self):
        return self._message


def load_config(path, config):
    """
    Loads the Flask configuration from a YAML file.

    :param path: Path to a configuration YAML file.
    :type path: string
    :param config: A dictionary that will hold the configuration.
    :type config: dict
    :raises YAMLError: If we cannot find the file, or it isn't a valid YAML file.
    """
    try:
        loaded_config = yaml.safe_load(open(path, 'r'))
    except MarkedYAMLError as e:
        raise YAMLError(e.problem)
    except FileNotFoundError:
        raise YAMLError('Cannot find config file "{0}"'.format(path))

    for group_name, settings in loaded_config.items():
        if group_name.upper() == 'GENERAL':
            for name, value in settings.items():
                config[name.upper()] = value
        else:
            for name, value in settings.items():
                n = '{0}_{1}'.format(group_name, name).upper()
                config[n] = value
