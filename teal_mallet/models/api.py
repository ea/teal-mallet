# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

import os
import logging
import uuid
from urllib.parse import urljoin
import re
from functools import partial
import base64

from geoalchemy2 import Geometry, shape
from flask_restful import fields
from sqlalchemy.orm import validates
import pyproj
from shapely import ops

from teal_mallet import db, app
from teal_mallet.util import british2xy, UUID

logger = logging.getLogger('teal-mallet')


class Station(db.Model):
    __tablename__ = 'stations'

    id = db.Column(UUID(), primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    reference = db.Column(db.String, index=True)
    region = db.Column(db.String, nullable=True)
    name = db.Column(db.String, nullable=True)
    ngr = db.Column(db.String, nullable=True)
    location = db.Column(Geometry('POINT', 27700))
    is_private = db.Column(db.Boolean, nullable=False, default=True)
    hmac_key = db.Column(db.String(44), nullable=False)

    resource_fields = {
        'id': fields.String,
        'owner_id': fields.Integer,
        'stationReference': fields.String(attribute='reference'),
        'region': fields.String,
        'stationName': fields.String(attribute='name'),
        'ngr': fields.String,
        'privacy': fields.String(attribute='is_private')
    }

    def __init__(self, reference, owner_id, region=None, name=None, ngr=None, is_private=True, use_hmac=False):
        self.id = uuid.uuid5(uuid.NAMESPACE_URL,
                             urljoin(app.config['UUID_NAMESPACE'], reference))
        self.reference = reference
        self.region = region
        self.name = name
        self.is_private = is_private
        self.owner_id = owner_id
        if use_hmac:
            self.hmac_key = base64.b64encode(os.urandom(32)).decode()
        else:
            self.hmac_key = ''

        try:
            ngr = ''.join(ngr.split()).upper()
            coords = british2xy(ngr)
            self.ngr = ngr
        except (IndexError, ValueError, AttributeError):
            coords = (0.0, 0.0)
            self.ngr = ''

        self.location = 'SRID=27700;POINT({0} {1})'.format(coords[0], coords[1])

    # noinspection PyUnusedLocal
    @validates('ngr')
    def validate_ngr(self, key, gridref):
        gridref = gridref.strip().upper()

        if re.match(r'[A-Z]{2}[0-9]{2,10}', gridref):
            return gridref
        else:
            return ''

    @property
    def lng_lat(self):
        s = shape.to_shape(self.location)
        xform = partial(pyproj.transform, pyproj.Proj(init='epsg:27700'), pyproj.Proj(init='epsg:4326'))
        p = ops.transform(xform, s)

        return p.x, p.y


class Metadata(db.Model):
    __tablename__ = 'metadata'

    id = db.Column(db.Integer, primary_key=True)
    publisher = db.Column(db.String)
    source = db.Column(db.String)
    description = db.Column(db.String)
    creator = db.Column(db.String)
    identifier = db.Column(db.String)

    def __init__(self, publisher, source, description, creator, identifier):
        self.publisher = publisher
        self.source = source
        self.description = description
        self.creator = creator
        self.identifier = identifier


class Entity(db.Model):
    __tablename__ = 'entity'

    id = db.Column(UUID(), primary_key=True)
