# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from teal_mallet import db
from flask.ext.security import UserMixin, RoleMixin

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('users.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('roles.id')))


class Role(db.Model, RoleMixin):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True)
    description = db.Column(db.String)

    def __init__(self, name, description=None):
        self.name = name
        self.description = description

    def __repr__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    need_password_change = db.Column(db.Boolean, nullable=False, default=True)
    active = db.Column(db.Boolean, nullable=False)
    roles = db.relationship('Role', secondary=roles_users, backref=db.backref('users'))

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def is_active(self):
        return True

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User - {0} ({1})>'.format(self.name, self.email)

    def __str__(self):
        return '{0} ({1})'.format(self.name, self.email)
