# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
import logging
from logging.handlers import HTTPHandler
import sys

import requests
from flask import Flask, request
from flask.ext.security import SQLAlchemyUserDatastore, Security
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext import login
from flask_mail import Mail
from sqlalchemy import orm
from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError

from teal_mallet.dashboard.forms import ExtendedRegisterForm
from teal_mallet.util import load_config


class InfluxDBHandler(HTTPHandler):
    def emit(self, record):
        try:
            if login.current_user.is_anonymous:
                user_name = 'Anonymous'
            else:
                user_name = login.current_user

            record.address = request.remote_addr
            record.user_name = user_name
        except (AttributeError, RuntimeError):
            # Ignore the request object if we're not logging a request.
            record.address = None
            record.user_name = None

        record.msg = record.msg.replace('"', r'\"')

        if record.levelno == logging.DEBUG:
            line = 'log,service=teal-mallet,level={levelname},addr={address},user={user_name},path={pathname},' \
                   'func="funcname",line={lineno} message="{msg}"'.format(**record.__dict__)
        else:
            line = 'log,service=teal-mallet,level={levelname},addr={address},user={user_name}' \
                   ' message="{msg}"'.format(**record.__dict__)

        requests.post('http://localhost:8086/write?db=teal_mallet', data=line.encode('utf-8'))

logging.basicConfig(level=logging.INFO)

# Create our own logger so that we can separate out our log messages from Flask's.
logger = logging.getLogger('teal-mallet')
logger.setLevel(logging.INFO)

influx_handler = InfluxDBHandler('localhost:8086', '/write?db=teal_mallet', method='POST')
logging.root.addHandler(influx_handler)

# Stop a potentially infinite loop of logging.
logging.getLogger('requests').setLevel(logging.WARNING)
logging.getLogger('requests').removeHandler(influx_handler)

app_name = __name__.split('.')[0]
logger.info('Creating Flask application "{}".'.format(app_name))

app = Flask(app_name)

load_config('teal_mallet/config/config.yaml', app.config)

log_level = app.config.get('LOG_LEVEL', 'WARNING')
numeric_level = getattr(logging, log_level.upper(), None)

if isinstance(numeric_level, int):
    logger.setLevel(numeric_level)
    logger.info('Switching logging level to {}.'.format(log_level))

# The secret key is used for CSRF handling, and other crypto stuff used by Flask.
if app.config.get('SECRET_KEY', None) is None:
    logger.info('Application secret key is not defined in config. Trying to load from file...')
    try:
        app.config['SECRET_KEY'] = open('teal_mallet/.secret', 'rb').read()
    except IOError:
        logger.error('Unable to find a secret key for this application. Terminating.')
        sys.exit(1)

logger.info('Application secret key found.')

db = SQLAlchemy(app)
influx_db = InfluxDBClient('127.0.0.1', 8086)

logger.info('Initialised database connection')

from teal_mallet.api import views as api_views
app.register_blueprint(api_views.api_blueprint, url_prefix='/api')

from teal_mallet.dashboard import views as dash_views
app.register_blueprint(dash_views.dashboard_blueprint)


logger.info('Imported blueprints')

from teal_mallet.models.api import Metadata
from teal_mallet.models.security import User, Role

# Set up Flask-Security.
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore, register_form=ExtendedRegisterForm)

db.create_all()
try:
    influx_db.create_database('teal_mallet')
except InfluxDBClientError:
    logger.info('Influx database already exists; skipping create.')

influx_db.switch_database('teal_mallet')
logger.info('Created tables')

mail = Mail(app)
logger.info('Mail system initialised')


@app.after_request
def gnu_terry_pratchett(resp):
    resp.headers.add('X-Clacks-Overhead', 'GNU Terry Pratchett')
    return resp


@app.before_first_request
def create_metadata():
    if Metadata.query.filter().count() == 0:
        m = Metadata(app.config['METADATA_PUBLISHER'], app.config['METADATA_SOURCE'],
                     app.config['METADATA_DESCRIPTION'], app.config['METADATA_CREATOR'],
                     app.config['METADATA_IDENTIFIER'])

        db.session.add(m)
        db.session.commit()


@app.before_first_request
def create_default_user():
    user_datastore.find_or_create_role(name='administrator',
                                       description='Special role able to create and remove users, '
                                                   'and has the rights of all other roles.')

    try:
        admin_user = User.query.filter(User.name == 'admin').one()

        if 'administrator' not in admin_user.roles:
            user_datastore.add_role_to_user(admin_user, 'administrator')

    except orm.exc.MultipleResultsFound:
        logger.error('We have more than one administrator in the database! This should be rectified immediately!')
        db.session.rollback()
        return
    except orm.exc.NoResultFound:
        logger.error('No admin user found in database.')
        user_datastore.create_user(name='admin', email='admin@example.com',
                                   password='admin',
                                   roles=['administrator'])

    db.session.commit()
