# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
import json

from flask import Blueprint, render_template, g, request, flash, url_for, redirect
from flask_security import login_required, current_user

from teal_mallet.api.exceptions import ResourceNotFoundError
from teal_mallet.models.api import Metadata, Station
from teal_mallet.dashboard.forms import MetadataForm, StationForm
from teal_mallet import app, db, logger
from teal_mallet.api import views as api_views

dashboard_blueprint = Blueprint('dashboard', __name__, template_folder='templates', static_folder='static')


@app.before_request
def before_request():
    g.current_user = current_user


@dashboard_blueprint.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@dashboard_blueprint.route('/summary', methods=['GET', 'POST'])
@login_required
def summary():
    form = StationForm()

    if form.validate_on_submit():
        new_station = Station(form.reference.data, current_user.id, region=form.region.data,
                              name=form.name.data, ngr=form.ngr.data, is_private=form.is_private.data,
                              use_hmac=form.use_hmac.data)
        db.session.add(new_station)
        db.session.commit()

        if form.use_hmac.data:
            flash(new_station.hmac_key, category='hmac')

        return redirect(url_for('dashboard.summary'))

    j = json.loads(api_views.station_summary('a3b40af1-16b5-4727-bcdc-60b0816a1e7b').get_data(as_text=True))

    # Put the current user's stations at the top of the list.
    s = [f for f in j['features'] if f['properties']['owner_id'] == current_user.id]
    user_stations = sorted(s, key=lambda k: k['properties']['stationReference'])
    s = [f for f in j['features'] if f['properties']['owner_id'] != current_user.id]
    other_stations = sorted(s, key=lambda k: k['properties']['stationReference'])

    return render_template('summary.html', stations=user_stations + other_stations, form=form,
                           maps_api_key=app.config['GOOGLE_MAPS_KEY'])


@dashboard_blueprint.route('/station', methods=['GET'])
@login_required
def station():
    station_id = request.args['id']

    old_accept = request.environ['HTTP_ACCEPT']

    request.environ['HTTP_ACCEPT'] = 'application/vnd.geo+json'
    try:
        j1 = json.loads(api_views.get_station('a3b40af1-16b5-4727-bcdc-60b0816a1e7b',
                                              station_id).get_data(as_text=True))
        station_data = j1['features'][0]
    except ResourceNotFoundError:
        station_data = None
    finally:
        request.environ['HTTP_ACCEPT'] = old_accept

    return render_template('station.html', station=station_data)


@dashboard_blueprint.route('/preferences', methods=['GET', 'POST'])
@login_required
def preferences():
    metadata = Metadata.query.filter().first()

    form = MetadataForm(obj=metadata)

    if form.validate_on_submit():
        flash('User name set to "{0}"'.format(form.creator.data))
        return redirect(url_for('dashboard.preferences'))

    return render_template('preferences.html', form=form)
