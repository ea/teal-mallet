# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from flask_wtf import Form, RecaptchaField
from flask_security.forms import RegisterForm
from flask_security import Security
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired, NoneOf


class MetadataForm(Form):
    publisher = StringField('Publisher', validators=[DataRequired()])
    creator = StringField('Creator', validators=[DataRequired()])  # TODO: Populate from Role


class ExportForm(Form):
    description = StringField('Description')
    source = StringField('Source')
    identifier = StringField('Identifier', validators=[DataRequired()])


class StationForm(Form):
    reference = StringField('Reference', validators=[DataRequired()])
    region = StringField('Region')
    name = StringField('Name')
    ngr = StringField('Grid Reference')
    # We default the boolean to false because checkbox states don't get submitted in form data if they're not checked.
    is_private = BooleanField('Private', default=False)
    use_hmac = BooleanField('Secure Header', default=False)


class ExtendedRegisterForm(RegisterForm):
    name = StringField('Name', validators=[DataRequired(), NoneOf(['admin'])])
    recaptcha = RecaptchaField()
