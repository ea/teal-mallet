# Copyright (c) 2015 GeoSpark and DPRV Ltd
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from teal_mallet import app

if __name__ == '__main__':
    app.run(host='10.0.2.15', debug=True)
