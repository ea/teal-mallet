# Installing Teal-Mallet

On the local machine you need Ansible >= 2.0 [installed](http://docs.ansible.com/ansible/intro_installation.html).

In the `deployment` directory you need a `hosts` file that points to the server or servers you wish to deploy on to. Ansible will use this, overriding any matching hosts in `/etc/ansible/hosts`

A couple of host variables need to be defined in the `hosts` file, so it should look something like this:

```
[teal-mallet]
<server FQDN or IP address>

[teal-mallet:vars]
ftp_user=<ftp user name>
ftp_password=<password for accessing the FTP server>
postgresql_user_password=<password for the teal-mallet PostgreSQL role>
postgresql_password_salt=<some string>
user_password_salt=<some string>
admin_email=admin@example.com
admin_password=admin
recaptcha_public_key=<your public recaptcha key>
recaptcha_private_key=<your private recaptcha key>
letsencrypt_email=<a contact email address>
welcome_message='<h2>Hello</h2><p>world!</p>'
google_maps_key=<your Google Maps API key>
metadata_publisher='Environment Agency'
metadata_source='North West Regional Telemetry System'
metadata_description='Automated telemetry data export process'
metadata_creator='SCX 6'
metadata_identifier='NWRFHSCXAS1'
email_server=<SMTP server address. Must support SSL>
email_username=<User name for SMTP server>
email_password=<Password for SMTP server>
```

On the remote hosts, you must already have ssh keys set up for the root user so Ansible can connect.
For more details, see "Setting up a Raspberry Pi" below.

To deploy to the remote hosts, from the command line run:

```
$ cd deployment
$ ansible-playbook server.yaml -i hosts --user root
```

Ansible will then install the system and dependencies. This can take 10 or 15 minutes, so be patient.

## Setting up a Raspberry Pi

On a raspberry pi with a default [raspbian-lite image](https://www.raspberrypi.org/downloads/raspbian/):

```
sudo adduser teal-mallet
```

You will be prompted for a user password. Remember this!

Then add the `teal-mallet` user to the sudoers file (so it can run sudo commands with a password). Run:

```
sudo visudo
```

To edit the file, and add the following in the line below `root    ALL=(ALL:ALL) ALL`:

```
teal-mallet    ALL=(ALL:ALL) ALL
```

(`Ctrl+X` to exit, then `Y` to save)

*It's also a good idea to change the default `pi`'s password at this point, otherwise 
anyone can log in to you're newly configured server with `root` access! Run `sudo raspi-config` while you're
still logged in.*

Then copy over your ssh key. On your local linux machine, you can use:

```
ssh-copy-id teal-mallet@raspberrypi.local
```

Which will ask you for the `teal-mallet` user's password to login this time. 
But otherwise take the contents of your `id_rsa.pub` file and append it to `teal-mallet`'s 
'/home/teal-mallet/.ssh/authorized_keys` file.



